def matrixGenerator(cols : int, rows : int) -> [[int]] :
    return [[0]*cols]*rows

def input_check(input_list : [int], size : int) -> bool :
    return sum(input_list) <= size
 
def take_input(size : int) -> [[]] :
    input_list = []
    i = 0
    while i < size :
        value = input("enter the value : ")
        value_list = [int(i) for i in value.split()]
        if input_check(value_list, size) :
          input_list.append(value_list)
          i += 1
        else :
            print("enter valid values")
    return input_list

def format_input(cols : int, rows : int) -> dict :
    Dict = {}
    Dict["cols"] = take_input(cols)
    Dict["rows"] = take_input(rows)
    return Dict

    